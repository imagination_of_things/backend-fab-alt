var express = require('express');
var router = express.Router();
const asyncMiddleware = require('../utils/utils');

var wm = require('../WordManipulator/index')
let wordManipulator = new wm();
let initialProcessing = require('./InitialProcessing')
let frameProcessing = require('./FrameProcessing')

var thesaurus = require('thesaurus-com');


// Home page route.
router.get('/mirror/:id', function (req, res) {
    let id = req.params.id
    wordManipulator.mirrorWords(id).then((data)=>{
        res.send(data);
    }).catch((err)=>{
        res.send(err);
    })
})

router.post('/initial', function(req, res){
  console.log(req.body);      // your JSON
  initialProcessing(req.body).then((sentences)=>{
    // console.log(words)
    res.send(sentences)

  }).catch((error)=>{
    res.send(error)
  })
});

router.post('/frame', function(req, res){
  console.log(req.body);      // your JSON
  frameProcessing(req.body).then((sentences)=>{
    // console.log(words)
    res.send(sentences)

  }).catch((error)=>{
    res.send(error)
  })
});

router.get('/sentence/:id', asyncMiddleware(async (req, res, next) => {
    /* 
      if there is an error thrown in getUserFromDb, asyncMiddleware
      will pass it to next() and express will handle the error;
    */
    const sentences = await wordManipulator.makeSentencesFromData(req.params.id)
    res.json(sentences);
}));

router.get('/concept/:id', asyncMiddleware(async (req, res, next) => {
  /* 
    if there is an error thrown in getUserFromDb, asyncMiddleware
    will pass it to next() and express will handle the error;
  */
  const concepts = await wordManipulator.generateConceptAndQuestions(req.params.id)
  res.json(concepts);
}));

router.get('/machinethink/:words', asyncMiddleware(async (req, res, next) => {
    /* 
      if there is an error thrown in getUserFromDb, asyncMiddleware
      will pass it to next() and express will handle the error;
    */
    const sentences = await wordManipulator.makeQuestionsFromRequest(req.params.words)
    res.json(sentences);
}));

router.get('/thesaurus/:words', asyncMiddleware(async (req, res, next) => {
    /* 
      if there is an error thrown in getUserFromDb, asyncMiddleware
      will pass it to next() and express will handle the error;
    */
    let words = req.params.words.split(",");
    let thesaurusData = []

    words.forEach(function(element) {
        console.log(element);
        let data = thesaurus.search(element)
        thesaurusData.push({
            word: element, 
            data: data
        })
    });
    res.json(thesaurusData);
}));


// About page route.
router.get('/about', function (req, res) {
  res.send('About this wiki');
})


module.exports = router;
