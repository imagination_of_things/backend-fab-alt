const axios = require('axios');
var FirebaseListener = require('../firebase/index');
let fbListener = new FirebaseListener();
let rita = require('rita')

var arrayUnique = function (arr) {
	return arr.filter(function(item, index){
		return arr.indexOf(item) >= index;
	});
};


module.exports = function(body) {
	console.log("FUNCTION")

	return new Promise((resolve, reject) => {
		let workshopRef = fbListener.db.collection('workshops').doc(body.id);
		
		let getDoc = workshopRef.get()
		  .then(doc => {
			if (!doc.exists) {
			  console.log('No such document!');
			} else {
			//   console.log('Document data:', doc.data());
			  let data = doc.data()
			  let userWords = data.userWords;
			   
			  

				let wordFreq = {}
				for(let i=0; i< userWords.length; i++) {
				  if(!wordFreq[userWords[i].word]) {
					wordFreq[userWords[i].word] = {}
					wordFreq[userWords[i].word]["count"] = 1;
					wordFreq[userWords[i].word]["data"] = userWords[i];
				  } else {
					  wordFreq[userWords[i].word]["count"] += 1;
					  wordFreq[userWords[i].word]["data"] = userWords[i];        }

					if(i === userWords.length-1) {
						console.log(wordFreq);
						let setDoc = fbListener.db.collection('workshops').doc(body.id).set({userWordsFreq: wordFreq}, {merge: true});
						resolve(wordFreq)
					}
				}






			}
		  })
		  .catch(err => {
			console.log('Error getting document', err);
			reject(err)
		  });

		  console.log(getDoc)
	});  
	
	
}


function calculateFreqsOfUserWords(userWords, id) {
	if(!userWords) { return {};}
     //   let userWords = this.workshops[localStorage.id].userWords;
      let wordFreq = {}
      for(let i=0; i< userWords.length; i++) {
        if(!wordFreq[userWords[i].word]) {
		  wordFreq[userWords[i].word]["count"] = 1;
		  wordFreq[userWords[i].word]["data"] = userWords[i];
        } else {
			wordFreq[userWords[i].word]["count"] += 1;
			wordFreq[userWords[i].word]["data"] = userWords[i];        }
      }
      console.log(wordFreq);
	  let setDoc = fbListener.db.collection('workshops').doc(id).set({userWordsFreq: wordFreq}, {merge: true});
	return wordFreq
    //console.log(this.backendWordInfo)



}