let questions = [
      'What if {{proper_noun}} was {{adverb}}?', 
      'What if {{proper_noun}} was {{a_noun}}?',    
      'What if {{proper_noun}} was {{past_verb}}?', 

      'What if {{plural_noun}} were {{adverb}}?',
      'What if {{plural_noun}} were {{plural_noun}}?',
      'What if {{plural_noun}} were {{past_verb}}?',

      'What if {{a_noun}} was {{adverb}}?',
      'What if {{a_noun}} was {{a_noun}}?',
      'What if {{a_noun}} was {{past_verb}}?',

      // 'What if {{proper_noun}} was {{'   

]

module.exports = questions;