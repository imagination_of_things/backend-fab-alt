var FirebaseListener = require('../firebase/index');

var Sentencer = require('sentencer');

var questions = require('./QuestionTemplates')

var rita = require('rita');

let prefab = require('./prefab.json')

let database = require('./database.json')

var thesaurus = require('thesaurus-com');

class WordManipulator {
    constructor() {
        this.fbListener = new FirebaseListener();
        this.data = [];

  
    }


    makeQuestionsFromRequest(words) {

        let data = words.split(',');

        Sentencer.configure({
            nounList: data
        })
    }

    getData() {

        return new Promise((resolve, reject)=> {
            this.data = [];

            this.fbListener.db.collection('workshops').get()
            .then((snapshot) => {
            snapshot.forEach((doc) => {
                this.data.push(doc);
            });
            resolve()
            })
            .catch((err) => {
            ////console.log('Error getting documents', err);
            reject()
            });
        })

     
    }



    //This is for invnentions section

    async generateConceptAndQuestions(id) {
        let data;
        let count = 0;
        let concepts = []
        let questions = [];

        let thesaurusData = {};

        let sentencerWordData = {}


        //We get word info from firebase.
        await this.getData()
        this.data.forEach((doc)=>{
            if(doc.id === id) {
                data = doc.data();
                console.log(data.wordInfo)

                for(let i=0; i<data.wordInfo.length; i++){
                    let tag = rita.RiTa.getPosTags(data.wordInfo[i].word)[0];
                    let databaseWord ='';
                    let databaseWords = [];


                
                    //Make a coin flip between adjective and past participle
                    //Generate 3 of each
                    if(Math.random()>0.4) {

                        for(let i=0; i<3; i++) {
                            let randIndex = Math.floor(Math.random()*database['vbn'].length);
                            databaseWords.push( {word: database["vbn"][randIndex], type: "vbn", weight: -1000, active: true} );
                        }

                    } else {
                        for(let i=0; i<3; i++) {
                            let randIndex = Math.floor(Math.random()*database['jj'].length);
                            databaseWords.push({word: database["jj"][randIndex], type: "jj", weight: -1000, active: true});
                        }
                    }


                    //Generate our concept

                    if(data.wordInfo[i].word.length >= 3) {

                        let important = rita.RiTa.singularize(data.wordInfo[i].word)
                        if(!thesaurusData[important]){
                            thesaurusData[important] = thesaurus.search(important);
                        }

                        // Based on weight

                        if(data.wordInfo[i].weight > 0) {

                            

                            // MAKE TWO CONCEPTS FOR IMPORANT WORDS
                            let concept = [{word: important, weight: data.wordInfo[i].weight/50, type:data.wordInfo[i].type, active: true}, {word: "is", type:"-", active: false}, databaseWords[0]]
                            concepts.push(concept)

                            if(!sentencerWordData[tag]) {
                                sentencerWordData[tag] = []
                            }
                    
                            sentencerWordData[tag].push({word: data.wordInfo[i].word, index:count})
                    
                            count+=1;

                            concept = [{word: important, weight: data.wordInfo[i].weight/50, type:data.wordInfo[i].type, active: true}, {word: "is", type:"-", active: false}, databaseWords[1]]
                            concepts.push(concept)

                            if(!sentencerWordData[tag]) {
                                sentencerWordData[tag] = []
                            }
                    
                            sentencerWordData[tag].push({word: data.wordInfo[i].word, index:count})
                    
                            count+=1;
                        } else {
                            let concept = [{word: important, weight: data.wordInfo[i].weight/50, type:data.wordInfo[i].type, active: true}, {word: "is", type:"-", active: false}, databaseWords[0]]
                            concepts.push(concept)
                            if(!sentencerWordData[tag]) {
                                sentencerWordData[tag] = []
                            }
                    
                            sentencerWordData[tag].push({word: data.wordInfo[i].word, index:count})
                    
                            count+=1;
                        }



           
                        // For each concept make a set of questions
                    }
              
                  
                 

                }
                  

            }
        })


        concepts.forEach((el, index)=>{
            let randIndex = Math.floor(Math.random()*thesaurusData[el[0].word].synonyms.length-1);
            let newWord = thesaurusData[el[0].word].synonyms[randIndex];
            let type =  rita.RiTa.getPosTags(newWord)[0]

            if( type === "nn" ) {
                var vowelRegex = '^[aieouAIEOU].*'
                var matched = newWord.match(vowelRegex)
                if(matched)
                {
                    newWord = `an ${newWord}`

                }
                else
                {
                    newWord = `a ${newWord}`

                }
            } else {
                newWord = thesaurusData[el[0].word].synonyms[randIndex];
            }

            let coinToss = Math.random();
            let secondType = '';

            let second = '';
            if(coinToss < 0.33) {
                let randIndex = Math.floor(Math.random()*database['vbn'].length);
                secondType = 'vbn'

                second = database['vbn'][randIndex]
            } else if (coinToss >= 0.33 && coinToss < 0.66) {
                let randIndex = Math.floor(Math.random()*database['jj'].length);
                secondType = 'jj'

                second = database['jj'][randIndex]
            } else {
                let randIndex = Math.floor(Math.random()*database['nn'].length);
                let word = database['nn'][randIndex];
                secondType = 'nn'
                var vowelRegex = '^[aieouAIEOU].*'
                var matched = word.match(vowelRegex)
                if(matched)
                {
                    second = `an ${word}`

                }
                else
                {
                    second = `a ${word}`

                }
            }
            

            if(!newWord) {
                console.warn(newWord, el)
                newWord = el[0].word
            }



            let question = [
                {word: "What if", active: false},
                {word: newWord, active: true, weight: -1000, type: type},
                {word: "was", active: false},
                {word: second, active: true, weight: -1000, type: secondType },
                {word: "?", active: false}

            ]
            questions.push(question);
            // ////console.log(el[0].word, thesaurusData[el[0].word].synonyms[randIndex])
            // el.question = 

        })


        //shuffle
        this.shuffleArrays(concepts, questions)

        return {id: id, concepts: concepts, questions: questions}

    }

     shuffleArrays(array, array2) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            var temp2 = array2[i]
            array[i] = array[j];
            array[j] = temp;
            array2[i] = array2[j];
            array2[j] = temp2;

        }
    }



    async generateConcept(id) {
        let data;
        let count = 0;
        let concepts = []
        let questions = [];

        let thesaurusData = {};

        let sentencerWordData = {}


        //We get word info from firebase.
        await this.getData()
        this.data.forEach((doc)=>{
            if(doc.id === id) {
                data = doc.data();
                console.log(data.wordInfo)

                for(let i=0; i<data.wordInfo.length; i++){
                    let tag = rita.RiTa.getPosTags(data.wordInfo[i].word)[0];
                    let databaseWord ='';
                    let databaseWords = [];

                    //This is our important word
                    let important = rita.RiTa.singularize(data.wordInfo[i].word)
                    if(!thesaurusData[important]){
                        thesaurusData[important] = thesaurus.search(important);
                    }

                        // Based on weight

                    if(data.wordInfo[i].weight > 0) {

                        




                    } else {
        
                    }



           
                }
              
                  
                 

            }
                  

        })

        return {id: id, questions: questions}

    }

    async makeSentencesFromData(id) {
            let data;
            let userAdjectives = [];
            let userNouns = [];
            let userVerbs = [];

            await this.getData()
            this.data.forEach((doc)=>{
                if(doc.id === id) {
                    data = doc.data();
                    for(let i=0; i<data.wordInfo.length; i++){

                          if (data.wordInfo[i].type === "n") {
                                userNouns.push(data.wordInfo[i])

                            } 
                     

                    }
                      

                }
            })



            

          

            Sentencer.configure({
                actions: {
                    userNoun: function() {

                        if(userNouns.length > 0) {
                            let index = Math.floor( Math.random() * userNouns.length )
                            return rita.RiTa.singularize(userNouns[index].word);
                        } else {
                            let index = Math.floor( Math.random() * prefab.nouns.length )

                            return prefab.nouns[index];
                        }
            
                    }
                }
            });

            let sentences = []
            let splits = [];
            let tags = [];

            for(let i=0; i<10; i++) {
                let randomIndex = Math.floor((Math.random()*questions.length))
                let randomQuestion = questions[randomIndex]
                let sentence = [];
                //sentences.push(Sentencer.make(randomQuestion))
                let q = Sentencer.make(randomQuestion)
                let split = rita.RiTa.tokenize(q);
                let tags = rita.RiTa.getPosTags(q, true);
                split.forEach((el,index)=>{


                     if (tags[index].includes("a") || tags[index].includes("n") || tags[index].includes("v")) {
                        let weight = this.checkIfInWordInfo(el, userNouns);
                        // let weight = null;
                        if(weight != null) {
                            sentence.push({word: el, type:tags[index], index: index, weight: weight/50, active: true})

                        } else {
                            sentence.push({word: el, type:tags[index], index: index, weight: 0, active: true})

                        }

                    } else {
                        sentence.push({word: el, type:tags[index], index: index, weight: 0, active: false})

                    }
                    
                })

                sentences.push(sentence);


            }

         

            let workshopRef = this.fbListener.db.collection('workshops').doc(id);

            let setWithOptions = workshopRef.set({
            sentences: sentences
            }, {merge: true});
            return sentences;
     
     
        
    }

    checkIfInWordInfo(word, userNouns) {

        for(let i=0; i<userNouns.length; i++) {
            ////console.log(word, userNouns[i].word)
            if(userNouns[i].word === word) {
                return userNouns[i].weight;
            }
        }

        // ////console.log(userNouns)

        return null;


    }

        async makeSentencesFromProblemStatements(id) {
             let data;
            await this.getData()
            this.data.forEach((doc)=>{
                if(doc.id === id) {
                    ////console.log(doc.data().wordBank);
                    data = doc.data();
                }
            })

            Sentencer.configure({
                nounList: data.wordBank
            })
            let sentences = []
            for(let i=0; i<25; i++) {
                let randomIndex = Math.floor((Math.random()*questions.length))
                let randomQuestion = questions[randomIndex]
                sentences.push(Sentencer.make(randomQuestion))
            }

            let workshopRef = this.fbListener.db.collection('workshops').doc(id);

            let setWithOptions = workshopRef.set({
            sentences: sentences
            }, {merge: true});

            return sentences;
     
     
        
     
     
        
    }

    
}














class Singleton {

  constructor() {
      if (!Singleton.instance) {
          Singleton.instance = new WordManipulator();
      }
  }

  getInstance() {
      return Singleton.instance;
  }

}

module.exports = WordManipulator