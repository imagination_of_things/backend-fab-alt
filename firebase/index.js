const admin = require('firebase-admin');
let serviceAccount = require('../credentials/serviceaccount.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

class FirebaseListener {

    constructor() {

        this.workshopData = []
        this.workshops = [];

        this.db = admin.firestore();
        this.observer = this.db.collection('workshops').onSnapshot(docSnapshot => {
            this.db.collection('workshops').get()

        }, err => {
            console.log(`Encountered error: ${err}`);
        });

        this.getWorkshopData();
    }

    async getWorkshopData() {
        console.log("asdf")
        try {
            let snapshot = this.db.collection('workshops').get()
            .then((snapshot) => {
                snapshot.forEach((doc) => {
                    this.workshopData.push(doc)
                    console.log(doc.id)
                });
            })
            .catch((err) => {
                console.log('Error getting documents', err);
            });

            return this.workshopData
        } catch (e) {

        }

    }
    getWorkshopData(id) {
        let workshopData = [];
        this.workshops.forEach((doc) => {
            if(doc.id === id) {
                            workshopData.push(doc.data());

            }
        })
        return workshopData; 
   

    }

}



module.exports = FirebaseListener