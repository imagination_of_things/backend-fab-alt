

var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var cors = require('cors');

let workshops = [];

var router = require('./router/index')
app.use(cors());
app.use(bodyParser());

app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "https://fabricating-alternatives.netlify.com"); // update to match the domain you will make the request from
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
  console.log("HTTP Get Request");
  res.send("Fabricating Alternatives Engine")
});

app.use('/api', router)



const port = process.env.PORT || 8080;
var server = app.listen(port, () => {
  console.log('Hello world listening on port', port);
});